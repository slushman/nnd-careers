<?php

/*
Plugin Name: NND Careers
Plugin URI: http://nursenextdoor.com/
Description: Manages careers info for Nurse Next Door and allows easy searching of existing locations to apply for jobs.
Version: 0.1
Author: Slushman
Author URI: http://www.slushman.com
License: GPLv2

**************************************************************************

  Copyright (C) 2013 Slushman

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General License for more details.templates/classic.php

  You should have received a copy of the GNU General License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************


!!! This plugin requires the Location Search plugin !!!


*/

if ( !class_exists( 'Slushman_NND_Careers' ) ) { //Start Class

	class Slushman_NND_Careers {
	
		public static $instance;
		const PLUGIN_NAME 	= 'NND Careers';
		const SETS_NAME		= 'slushman_nndc_settings';
		const SETS_SLUG		= 'slushman-nndc-settings';
		const PLUGIN_SLUG	= 'slushman-nndc';
		const CPT_NAME		= 'slushman_cpt_careers';
		const CPT_PLURAL	= 'Careers';
		const CPT_SINGLE	= 'Career';
	
/**
 * Constructor
 */
		function __construct() {
		
			self::$instance = $this;

			// Runs when plugin is activated
			register_activation_hook( __FILE__, array( $this, 'install' ) );
			
			// Create shortcode
			add_shortcode( 'nndcareersearch', array( $this, 'shortcode' ) );
			
			// Adds the plugin settings to the Settings menu
			add_action( 'admin_menu', array( $this, 'add_menu' ) );
			
			//	Add "Settings" link to plugin page
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ) , array( $this, 'settings_link' ) );
			
			// Register and define the settings
			add_action( 'admin_init', array( $this, 'settings_reg' ) );

			// Create custom post type
			add_action( 'init', array( $this, 'create_cpt' ) );

			$this->options 	= (array) get_option( self::SETS_NAME );

			// 0: section, 1: underscored name, 2: type, 3: default value, 4: description, 5: selections	
			$this->fields[0] 	= array( 'settings', 'default_page', 'select', '', 'Choose the default careers page.', $this->get_career_pages_sels() );

			$this->cpt['plural'] = 'Careers';
			$this->cpt['single'] = 'Career';
		
		} // End of __construct()
		
/**
 * Creates the plugin settings
 *
 * Creates an array containing each setting and sets the default values to blank.
 * Then saves the array in the plugin option.
 *
 * @since	0.1
 * 
 * @uses	settings_init
 */	
		function install() {
		
			//$this->settings_init();
			
		} // End of install()


		
/* ==========================================================================
   Plugin Settings
   ========================================================================== */

/**
 * Creates the plugin settings
 *
 * Creates an array containing each setting and sets the default values to blank.
 * Then saves the array in the plugin option.
 *
 * @since	0.1
 * 
 * @uses	update_option
 */		
		function settings_init() {
		
			$settings = array();

			foreach ( $this->fields as $field ) {

				$settings[$field[1]] = $field[3]; 

			} // End of $fields foreach loop

			update_option( self::SETS_NAME, $settings );
			
		} // End of settings_init()  

/**
 * Adds the plugin settings page to the appropriate admin menu
 *
 * @since	0.1
 * 
 * @uses	add_options_page
 */	
		function add_menu() {
		
			add_submenu_page(
				'edit.php?post_type=' . self::CPT_NAME,
				__( self::PLUGIN_NAME . ' Settings' ),
				__( 'Settings' ),
				'edit_posts',
				self::PLUGIN_SLUG . '-settings',
				array( $this, 'settings_page' )
			);
		
		} // End of add_menu()
		
/**
 * Adds a link to the plugin settings page to the plugin's listing on the plugin page
 *
 * @since	0.1
 * 
 * @uses	admin_url
 */
		function settings_link( $links ) {
		
			$settings_link = sprintf( '<a href="%s">%s</a>', admin_url( 'options-general.php?page=' . self::PLUGIN_SLUG ), __( 'Settings' ) );
		
			array_unshift( $links, $settings_link );
		
			return $links;
		
		} // End of settings_link()
		
/**
 * Creates the settings page
 *
 * @since	0.1
 *
 * @uses	plugins_url
 * @uses	settings_fields
 * @uses	do_settings_sections
 * @uses	submit_button
 */
		function settings_page() {
			
			$plugin = get_plugin_data( __FILE__ ); ?>
			
			<div class="wrap">
			<div class="icon32 slushman_nndc_logo" style="background: transparent url(<?php echo plugins_url( 'images/logo.png', __FILE__ ); ?>) no-repeat center center;"></div>
			<h2><?php echo $plugin['Name']; ?></h2>
			<form method="post" action="options.php"><?php
			
				settings_fields( self::SETS_NAME );
				do_settings_sections( self::SETS_NAME );
				echo '<br />'; 
				submit_button(); ?>
				
			</form>
			</div><?php
						
		} // End of settings_page()

/**
 * Writes the header for the Fallback section
 */
		function settings_settings_fn() {
		

		
		} // End of fallback_settings_fn()
				
/**
 * Registers the plugin option, settings, and sections
 *
 * Instead of writing the registration for each field, I used a foreach loop to write them all.
 * add_settings_field has an argument that can pass data to the callback, which I used to send the specifics
 * of each setting field to the callback that actually creates the setting field. 
 *
 * @since	0.1
 * 
 * @uses	register_setting
 * @uses	add_settings_section
 * @uses	add_settings_field
 */	
		function settings_reg() {
			
			register_setting( 
				self::SETS_NAME, 
				self::SETS_NAME,
				array( $this, 'validate_options' )
			);
			
			$prefix 	= 'slushman_nndc_';
			$sections 	= array( 'Settings' );
			
			foreach ( $sections as $section ) {
			
				$secname = strtolower( str_replace( ' ', '_', $section ) );
			
				add_settings_section( 
					$prefix . $secname, 
					$section . ' Settings', 
					array( $this, $secname . '_settings_fn' ), 
					self::SETS_NAME
				);
				
			} // End of $sections foreach
			
			foreach ( $this->fields as $field ) {
			
				$corv 	= ( $field[2] == 'checkbox' ? 'check' : 'value' );
				$dorl	= ( $field[2] == 'checkbox' ? 'label' : 'desc' );
				$desc 	= ( !empty( $field[4] ) ? $field[4] : '' );
				$sels	= ( !empty( $field[5] ) ? $field[5] : array() );
				
				$setsargs['blank']		= ( $field[2] == 'select' ? TRUE : '' );
				$setsargs['class']		= $prefix . $field[1];
				$setsargs['id'] 		= $field[1];
				$setsargs['name'] 		= self::SETS_NAME . '[' . $field[1] . ']';
				$setsargs['type'] 		= $field[2];
				$setsargs[$corv] 		= ( !empty( $this->options[$field[1]] ) ? $this->options[$field[1]] : '' );
				$setsargs[$dorl] 		= $desc;
				$setsargs['selections'] = $sels;
				
				add_settings_field(
					$prefix . $field[1] . '_field', 
					ucwords( str_replace( '_', ' ', $field[1] ) ), 
					array( $this, 'create_settings_fn' ), 
					self::SETS_NAME,
					$prefix . $field[0],
					$setsargs
				);
					
			} // End of $fields foreach
			
		} // End of settings_reg()

/**
 * Creates the settings fields
 *
 * Accepts the $params from settings_reg() and creates a setting field
 *
 * @since	0.1
 *
 * @params	$params		The data specific to this setting field, comes from settings_reg()
 * 
 * @uses	checkbox
 */	
 		function create_settings_fn( $params ) {
 		
 			$check = array( 'blank', 'check', 'class', 'desc', 'id', 'label', 'name', 'select', 'type', 'value' );
 			
 			foreach ( $check as $field ) {
	 			
	 			$args[$field] = ( !empty( $params[$field] ) ? $params[$field] : '' );
	 			
 			} // End of $params foreach
 			
 			$args['selections'] = ( isset( $params['selections'] ) ? $params['selections'] : '' );
		
 			extract( $args );
 			
 			switch ( $type ) {
	 			
	 			case ( 'email' )		:
	 			case ( 'file' )			:
	 			case ( 'number' )		:
	 			case ( 'tel' ) 			: 
	 			case ( 'url' ) 			: 
	 			case ( 'text' ) 		: echo $this->input_field( $args ); break;
	 			case ( 'checkbox' ) 	: echo $this->checkbox( $args ); break;
	 			case ( 'textarea' )		: echo $this->textarea( $args ); break;
	 			case ( 'checkgroup' ) 	: echo $this->make_checkboxes( $args ); break;
	 			case ( 'radios' ) 		: echo $this->make_radios( $args ); break;
	 			case ( 'select' )		: echo $this->make_select( $args ); break;
	 			
 			} // End of $inputtype switch
			
		} // End of create_settings_fn()

/**
 * Validates the plugin settings before they are saved
 *
 * Loops through each plugin setting and sanitizes the data before returning it.
 *
 * @since	0.1
 */				
		function validate_options( $input ) {
		
			foreach ( $this->fields as $field ) {
			
				$name = $field[1];
			
				switch ( $field[2] ) {
	 			
		 			case ( 'email' )		: $valid[$name] = sanitize_email( $input[$name] ); break;
		 			case ( 'number' )		: $valid[$name] = intval( $input[$name] ); break;
		 			case ( 'url' ) 			: $valid[$name] = esc_url( $input[$name] ); break;
		 			case ( 'text' ) 		: $valid[$name] = sanitize_text_field( $input[$name] ); break;
		 			case ( 'textarea' )		: $valid[$name] = esc_textarea( $input[$name] ); break;
		 			case ( 'checkgroup' ) 	: $valid[$name] = strip_tags( $input[$name] ); break;
		 			case ( 'radios' ) 		: $valid[$name] = strip_tags( $input[$name] ); break;
		 			case ( 'select' )		: $valid[$name] = strip_tags( $input[$name] ); break;
		 			case ( 'tel' ) 			: $valid[$name] = $this->sanitize_phone( $input[$name] ); break;
		 			case ( 'checkbox' ) 	: 
		 				$valid[$name] = ( !isset( $input[$name] ) ? $this->options[$name] : $input[$name] );
		 				$valid[$name] = ( isset( $input[$name] ) && $input[$name] ? true : false ); 
		 				break;
		 			
	 			} // End of $inputtype switch
			
			} // End of $checks foreach

			return $valid;
		
		} // End of validate_options()



/* ==========================================================================
   Custom Post Types and Taxonomies
   ========================================================================== */		
		
/**
 * Creates custom post type staff_directory
 *
 * @since	0.1
 *
 * @uses	register_post_type
 */		
		function create_cpt() {

			$args['capability_type'] 				= 'post';
    		$args['hierarchical'] 					= FALSE;
    		$args['menu_position']  				= '25';
    		$args['public'] 						= TRUE;
			$args['publicly_querable']				= TRUE;
			$args['query_var']						= TRUE;
			$args['rewrite']						= FALSE;
			$args['rewrite']['slug'] 				= strtolower( $this->cpt['single'] );
			$args['rewrite']['with_front'] 			= FALSE;
			$args['show_ui']						= TRUE;
			$args['show_in_menu'] 					= TRUE;
			$args['supports'] 						= array( 'title', 'editor' );
			
			$args['labels']['name'] 				= _x( $this->cpt['plural'], 'post type general name' );
			$args['labels']['singular_name'] 		= _x( $this->cpt['single'], 'post type singular name' );
			$args['labels']['menu_name']			= __( $this->cpt['plural'], 'post type general name' );
			$args['labels']['all_items'] 			= __( $this->cpt['plural'] );
			$args['labels']['add_new'] 				= __( 'Add New ' . $this->cpt['single'] );
			$args['labels']['add_new_item'] 		= __( 'Add New ' . $this->cpt['single'] );
			$args['labels']['edit_item'] 			= __( 'Edit ' . $this->cpt['single'] );
			$args['labels']['new_item'] 			= __( 'New ' . $this->cpt['single'] );
			$args['labels']['view_item'] 			= __( 'View ' . $this->cpt['single'] );
			$args['labels']['search_items'] 		= __( 'Search ' . $this->cpt['plural'] );
			$args['labels']['not_found'] 			= __( 'No ' . $this->cpt['single'] . ' Found' );
			$args['labels']['not_found_in_trash'] 	= __( 'No ' . $this->cpt['single'] . ' Found in Trash' );
			
			// Register Albums custom post type
			register_post_type( self::CPT_NAME, $args );
			
			flush_rewrite_rules();
		
		} // End of create_cpt()



/* ==========================================================================
	Plugin Functions
========================================================================== */

/**
 * Fetches an array of locations based on the zip code. Coverts alphanumeric
 * zip codes to the first three characters before using WP_Query.
 *
 * @since 0.1
 *
 * @param	string			$input		The form input
 *
 * @uses	WP_Query
 *
 * @return	array | bool	$return		Either an array of location posts objects or FALSE if none are found
 */
		function db_search_first ( $input ) {

			$args['post_type'] 					= 'ls-location';
			$args['posts_per_page'] 			= '-1';
			$args['post_status'] 				= 'publish';

			if ( $input['nndc_zip'] != '' ) {

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_zip';
				$args['meta_query'][0]['value']		= ( is_numeric( $input['nndc_zip'] ) ? $input['nndc_zip'] : substr( $input['nndc_zip'], 0, 3 ) );
				$args['meta_query'][0]['compare']	= 'LIKE';

			} elseif ( $input['nndc_city'] != '' && $input['nndc_state'] != '' ) {

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_city';
				$args['meta_query'][0]['value']		= $input['nndc_city'];
				$args['meta_query'][0]['compare']	= 'LIKE';
				$args['meta_query'][1]['key']		= 'location_state';
				$args['meta_query'][1]['value']		= $input['nndc_state'];
				$args['meta_query'][1]['compare']	= 'LIKE';

			} elseif ( $input['nndc_city'] != '' || $input['nndc_state'] != '' ) {

				$which = ( $input['nndc_city'] != '' ? 'city' : 'state' );

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_' . $which;
				$args['meta_query'][0]['value']		= $input[$which];
				$args['meta_query'][0]['compare']	= 'LIKE';

			} // End input checks

			$locations = new WP_Query( $args );

			if ( $locations->post_count < 1 ) { return FALSE; }

			return $locations;

		} // End of db_search_first()

/**
 * Returns an array of the career CPT post objects
 * 
 * @uses	WP_Query
 *
 * @return 	bool | array 	$sels		FALSE if none are found, or an select array of careers labels and values
 */
		function get_career_pages_sels() {

			$args['post_type'] 					= 'slushman_cpt_careers';
			$args['posts_per_page'] 			= '-1';
			$args['post_status'] 				= 'publish';

			$careers = new WP_Query( $args );

			if ( !$careers || $careers->post_count < 1 ) { return FALSE; }

			$i 		= 0;
			$sels 	= array();

			foreach ( $careers->posts as $career ) {

				$permalink	= get_permalink( $career->ID );
				$sels[$i] 	= array( 'label' => $career->post_title, 'value' => $permalink );
				$i++;

			} // End of $careers foreach loop

			return $sels;

		} // End of get_career_pages()		

/**
 * search_form function.
 *
 * @param 	string		$mode		The name for the mode hidden field
 *
 * @uses	none_empty
 * @uses	zip_codes_first
 * @uses	get_post_custom
 * @uses	send_wp_mail
 * @uses	get_permalink
 * @uses	input_field
 * @uses	textarea
 * @uses	hidden_field
 * @uses	wp_nonce_field
 * @uses	esc_attr
 */
		function search_form() {

			if ( isset( $_POST['mode'] ) && $_POST['mode'] == 'careersearch' && ( !empty( $_POST['nndc_zip'] ) || !empty( $_POST['nndc_city'] ) && !empty( $_POST['nndc_state'] ) ) ) {

				$fields = array( 'nndc_zip', 'nndc_city', 'nndc_state' );
				$form 	= array();
				
				foreach ( $fields as $field ) {
					
					$form[$field] = ( !empty( $_POST[$field] ) ? $_POST[$field] : '' );
					
				} // End of $fields foreach loop

				$found = $this->db_search_first( $form );

				//$this->print_array( $found );

				if ( !$found || $found->post_count == 0 ) { ?>

					<script type="text/javascript">
					<!--
					window.location = <?php echo $this->options['default_page']; ?>
					//-->
					</script><?php

				} elseif ( $found->post_count == 1 ) {

					$custom = get_post_custom( $found->posts[0]->ID );
					$page = ( empty( $custom['career_page_select'][0] ) ? $this->options['default_page'] : $custom['career_page_select'][0] ); ?>
					<script type="text/javascript">
					<!--
					window.location = <?php echo '"' . $page . '"'; ?>
					//-->
					</script><?php

				} else {

					echo '<div id="careerresults">';

					foreach ( $found->posts as $post ) {

						$custom = get_post_custom( $found->posts[0]->ID );

						echo '<p class="careerlocation"><a href="' . $custom['career_page_select'][0] . '" class="careerlink">' . $post->post_title . '</a></p>';

					} // End of posts foreach loop

					echo '</div><!-- End of #results -->';

				}
				
			} elseif ( isset( $_POST['mode'] ) && $_POST['mode'] == 'careersearch' && ( empty( $_POST['nndc_zip'] ) || empty( $_POST['nndc_city'] ) || empty( $_POST['nndc_state'] ) ) ) {
				
				echo '<p class="nddc_outcome error">Please fill out either the zip or the city and state fields.</p>';
				
			} // End of mode check ?>

			<form method="post" action="<?php echo get_permalink(); ?>" id="findacareer"><?php

			$input_args['class'] = $input_args['id'] = $input_args['name'] = 'nndc_zip';
			$input_args['type']			= 'text';
			$input_args['placeholder'] 	= 'Zip / Postal Code';
			
			echo '<p>' . $this->input_field( $input_args ) . '</p>';
			
			$input_args['class'] = $input_args['id'] = $input_args['name'] = 'nndc_city';
			$input_args['type']			= 'text';
			$input_args['placeholder'] 	= 'City';
			
			echo '<p>' . $this->input_field( $input_args ) . '</p>';
			
			$input_args['class'] = $input_args['id'] = $input_args['name'] = 'nndc_state';
			$input_args['type']			= 'tel';
			$input_args['placeholder'] 	= 'State';
			
			echo '<p>' . $this->input_field( $input_args ) . '</p>';
			
			echo $this->hidden_field( array( 'name' => 'mode', 'value' => 'careersearch' ) );
			
			$submit = esc_attr( 'Submit' );
			
			echo '<input type="submit" name="' . $submit .'" id="' . $submit .'" class="button sel_submit" value="' . $submit . '" />';
			
			echo '</form>';
			
		} // End of search_form()

/**
 * Creates shortcode [email_location]
 * 
 * @uses	get_option
 * @uses	get_layout
 *
 * @return	mixed	$output		Output of the buffer
 */
		function shortcode( $atts ) {
		
			ob_start();
			
			$this->search_form();
			
			$output = ob_get_contents();
			
			ob_end_clean();
			
			return $output;
		
		} // End of shortcode()


/* ==========================================================================
   Slushman Toolkit Functions
   ========================================================================== */

/**
 * Creates an hidden field based on the params
 *
 * @params are:
 *  name - (optional), can be a separate value from ID
 *	value - used for the value attribute
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the hidden field
 *
 * @return	mixed	$output		A properly formatted HTML hidden field
 */			
		function hidden_field( $params ) { 
		
			extract( $params );
						
			$showname 	= ( !empty( $name ) ? ' name="' . $name . '"' : '' );
			$output 	= '<input type="hidden"' . $showname . 'value="' . ( !empty( $value ) ? $value : '' ) . '"' . ' />';
			
			return $output;
			
		} // End of hidden_field()

/**
 * Creates an input field based on the params
 *
 * Creates an input field based on the params
 * 
 * @params are:
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 * 	id - used for the id and name attributes
 *	label - the label to use in front of the field
 *  name - (optional), can be a separate value from ID
 *  placeholder - The text that appears in th field before a value is entered.
 *  type - detemines the particular type of input field to be created
 *	value - used for the value attribute
 * 
 * Inputtype options: 
 *  email - email address
 *  file - file upload
 *  text - standard text field (default)
 *  tel - phone numbers
 *  url - urls
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the text field
 *
 * @return	mixed	$output		A properly formatted HTML input field with optional label and description
 */			
		function input_field( $params ) { 
		
			extract( $params );
						
			$showid 	= ( !empty( $id ) ? ' id="' . $id . '" name="' . ( !empty( $name ) ? $name : $id ) . '"' : '' );
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			$showtype	= ( !empty( $type ) ? ' type="' . $type . '"' : 'text' );
			$showvalue	= ( !empty( $value ) ? ' value="' . ( $type == 'url' ? esc_url( $value ) : esc_attr( $value ) ) . '"' : 'value=""' );
			$showph		= ( !empty( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '' );
			
			$output 	= ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output 	.= '<input' . $showtype . $showid . $showvalue . $showclass . $showph . ' />';
			$output 	.= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
			
		} // End of input_field()

/**
 * Creates a select menu based on the params
 *
 * @params are:
 *  blank - false for none, true if you want a blank option, or enter text for the blank selector
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 * 	id - used for the id and name attributes
 *	label - the label to use in front of the field
 *  name - the name of the field
 *	value - used in the selected function
 *	selections - an array of data to use as the selections in the menu
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the select menu
 *
 * @return	mixed	$output		A properly formatted HTML select menu with optional label and description
 */	
		function make_select( $params ) {
			
			extract( $params );
			
			$showid 	= ( !empty( $id ) ? ' id="' . $id . '"' : '' );
			$showname 	= ' name="' . ( !empty( $name ) ? $name : ( !empty( $id ) ? $id : '' ) ) . '"';
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			
			$output = ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output .= '<select' . $showid . $showname . $showclass .'>';
			$output .= ( !empty( $blank ) ? '<option>' . ( !is_bool( $blank ) ? __( $blank ) : '' ) . '</option>' : '' );
			
			if ( !empty( $selections ) ) {
			
				foreach ( $selections as $selection ) {
				
					extract( $selection, EXTR_PREFIX_ALL, 'sel' );
				
					$optvalue 	= ( !empty( $sel_value ) ? ' value="' . $sel_value . '"' : '' );
					$output 	.= '<option' . $optvalue . selected( $value, $sel_value, FALSE ) . ' >' . $sel_label . '</option>';
					
				} // End of $selections foreach
				
			} // End of $selections empty check
			
			$output .= '</select>';
			$output .= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
						
		} // End of make_select()

/**
 * Display an array in a nice format
 *
 * @param	array	The array you wish to view
 */			
		public function print_array( $array ) {

		  echo '<pre>';
		  
		  print_r( $array );
		  
		  echo '</pre>';
		
		} // End of print_array()
		
/**
 * Creates an HTML textarea
 *
 * @params are:
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 *  id - used for the id and name attributes
 *  name - (optional), can be a separate value from ID
 *	label - the label to use in front of the field
 *  placeholder - The text that appears in th field before a value is entered.
 *	value - used in the checked function
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the textarea
 *
 * @return	mixed	$output		A properly formatted HTML textarea with optional label and description
 */
		function textarea( $params ) {
			
			extract( $params );
						
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			$showid 	= ( !empty( $id ) ? '" id="' . $id . '" name="' . ( !empty( $name ) ? $name : $id ) . '"' : '' );
			$showph		= ( !empty( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '' );
			$showvalue	= ( !empty( $value ) ? esc_textarea( $value ) : '' );
			$style 		= 'cols="50" rows="10" wrap="hard"';
			
			$output 	= ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output 	.= '<textarea ' . $showid . $showclass . $showph . $style . '>' . $showvalue . '</textarea>';
			$output 	.= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
			
		} // End of textarea()   

	} // End of Slushman_NND_Careers
		
} // End of class check

$nndcareers = new Slushman_NND_Careers;

?>